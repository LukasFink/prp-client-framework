/*
        PRP Project
        Copyright (C) 2020  The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope ref it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const DEFAULT_PROVIDER = {
    providerUrl: "http://prp.error-cloud.com:8081",
    authorizationEndpoint: "/oauth/token",
    tokenEndpoint: "/oauth/token",
    revokeTokenEndpoint: "/oauth/revoke",
    loginEndpoint: "/login"
};

const DEFAULT_CLIENT = {
    id: "CordovaWeb",
    secret: "secret",
    redirectURL: "http://prp.error-cloud.com",
    scope: "login"
};

class OAuth2 {

    constructor(localStorageManager, translationLoader) {
        this.logger = new Logger("OAuth2");
        this.localStorageManager = localStorageManager;
        this.trans = translationLoader;

        this.provider = DEFAULT_PROVIDER;

        let storedClient = this.localStorageManager.loadOauthClient();
        if (storedClient != null) {
            this.client = storedClient;
        } else {
            this.client = DEFAULT_CLIENT;
        }

        if (this.localStorageManager.loadAccessToken() == null) {
            // show loginform
            this.setLoginTextContent();
            $('#loginModal').modal('toggle');

            // add Eventlistener on submit
            document.getElementById('login').addEventListener('submit', this.login.bind(this));
        } else {
            this.authorizationHeader = OAuth2.authHeader(this.localStorageManager.loadAccessToken());

            try {
                this.loginTokenCheck();
            } catch (e) {
                this.logger.error(e);
            }
        }

        this.logger.log("Initialized OAuth2 manager.");
    }

    static authHeader(token) {
        let header = {authorization: "Bearer " + token};
        console.log("Created header: " + JSON.stringify(header));
        return header;
    }

    setLoginTextContent() {
        document.getElementById("loginTitle").textContent = this.trans.login.login;
        document.getElementById("username").setAttribute("placeholder", this.trans.login.username);
        document.getElementById("password").setAttribute("placeholder", this.trans.login.password);
        document.getElementById("loginUsernameLabel").textContent = this.trans.login.username;
        document.getElementById("loginPasswordLabel").textContent = this.trans.login.password;
        document.getElementById("loginRememberMeLabel").innerHTML += this.trans.login.rememberMe;
        document.getElementById("loginButtonLoginModal").textContent = this.trans.login.login;
    }

    async login(event) {
        event.preventDefault();

        //build URL for token request
        let url = this.provider.providerUrl + this.provider.authorizationEndpoint + '?';
        url += 'username=' + encodeURI(document.getElementById('username').value);
        url += '&password=' + encodeURI(document.getElementById('password').value);
        url += '&response_type=token&grant_type=password';

        let ref = this;
        let headers = {
            'Authorization': 'Basic ' + btoa(this.client.id + ':' + this.client.secret)
        }

        //send request
        try {
            await $.ajax({
                url: url,
                type: "POST",
                headers: headers,
                success: function (data) {
                    ref._assignAuthHeader(data.access_token);
                    ref.localStorageManager.storeAccessToken(data.access_token);
                    ref.logger.log("Getting Login-Token successful");
                    $('#loginModal').modal('toggle');
                    window.location.reload();
                },
                error: function (data, textStatus, errorThrown) {
                    ref.logger.error("Getting Login-Token Failed jqXHR: " + JSON.stringify(data));
                    document.getElementById('loginButtonLoginModal').classList.add("errorButton");
                    document.getElementById('errorLoginModal').textContent = ref.trans.prpClientFramework.loginErrorLabel;
                    document.getElementById('errorLoginModal').hidden = false;
                }
            });
        } catch (e) {
            ref.logger.error(e);
        }
    }

    _assignAuthHeader(token) {
        this.authorizationHeader = OAuth2.authHeader(token);
    }

    async loginTokenCheck() {
        let ref = this;
        let url = "/oauth/check_token?token=" + this.localStorageManager.loadAccessToken();

        try {
            await $.ajax({
                url: this.provider.providerUrl + url,
                method: "GET",
                headers: {
                    "Authorization": "Basic " + btoa(ref.client.id + ':' + ref.client.secret)
                },
                success: function (data) {
                    ref.logger.log(JSON.stringify(data));
                },
                error: function (data, textStatus, errorThrown) {
                    if (data.responseJSON.error_description === "Token has expired") {
                        ref.logger.log("Token expired, removing Token");
                        ref.localStorageManager.clearAll();
                        window.location.reload();
                    } else {
                        ref.logger.error("Getting User Date for Login-Check Failed: " + JSON.stringify(data) + " " + textStatus + " " + errorThrown);
                    }
                }
            });
        } catch (e) {
            this.logger.error("Token check failed.");
            this.logger.error(e);
            this.localStorageManager.clearAll();
        }
    }

}
