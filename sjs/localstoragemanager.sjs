/*
        PRP Project
        Copyright (C) 2020  The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class LocalStorageManager {

    constructor() {
        this.logger = new Logger("LocalStorageManager");
        this.logger.log("Initialized LocalStorageManager: " + JSON.stringify(window.localStorage));
    }

    loadAccessToken() {
        return this._load("access_token");
    }

    storeAccessToken(token) {
        this._store("access_token", token);
    }

    removeAccessToken() {
        this._remove("access_token");
    }

    loadRefreshToken() {
        return this._load("refresh_token");
    }

    storeRefreshToken(token) {
        this._store("refresh_token", token);
    }

    removeRefreshToken() {
        this._remove("refresh_token");
    }

    loadOauthClient() {
        let client = {
            id: this._load("oauth_client_id"),
            secret: this._load("oauth_client_secret"),
            redirectURL: this._load("oauth_client_redirect_url"),
            scope: this._load("oauth_client_scope")
        };

        if (client.id == null) {
            client.id = "CordovaWeb";
        }

        if (client.secret == null) {
            client.secret = "secret";
        }

        if (client.redirectURL == null) {
            client.redirectURL = "http://prp.error-cloud.com";
        }

        if (client.scope == null) {
            client.scope = "login";
        }

        return client;
    }

    storeOauthClient(id, secret, redirectUrl, scope) {
        this._store("oauth_client_id", id);
        this._store("oauth_client_secret", secret);
        this._store("oauth_client_redirect_url", redirectUrl);
        this._store("oauth_client_scope", scope);
    }

    removeOauthClient() {
        this._remove("oauth_client_id");
        this._remove("oauth_client_secret");
        this._remove("oauth_client_redirect_url");
        this._remove("oauth_client_scope");
    }

    loadResourceServerUrl() {
        let resourceServer = this._load("resource_server_url");
        if (resourceServer == null) {
            resourceServer = "http://localhost:8080";
        }
        return resourceServer;
    }

    storeResourceServerUrl(url) {
        this._store("resource_server_url", url);
    }

    removeResourceServerUrl() {
        this._remove("resource_server_url");
    }

    loadOauthProvider() {
        let provider = this._load("oauth_provider_base_url");
        if (provider == null) {
            provider = "http://localhost:8081";
        }
        return provider;
    }

    storeOauthProvider(url) {
        this._store("oauth_provider_base_url", url);
    }

    removeOauthProvider() {
        this._remove("oauth_provider_base_url");
    }

    clearAll() {
        this.logger.warn("Removing all data.");
        this._remove("access_token");
        this._remove("refresh_token");
        this._remove("oauth_client_id");
        this._remove("oauth_client_secret");
        this._remove("oauth_client_redirect_url");
        this._remove("oauth_client_scope");
        this._remove("oauth_provider_base_url");
        this._remove("resource_server_url");
        this.logger.log("Removed all data. Still available: " + JSON.stringify(window.localStorage));
    }

    _load(key) {
        let loaded = window.localStorage.getItem(key);
        this.logger.log("Loaded from local storage {" + key + ": " + JSON.stringify(loaded) + "}");
        return loaded;
    }

    _store(key, value) {
        window.localStorage.setItem(key, value);
        this.logger.log("Stored into local storage {" + key + ": " + JSON.stringify(value) + "}");
        this.logger.log("All stored values: " + JSON.stringify(window.localStorage));
    }

    _remove(key) {
        window.localStorage.removeItem(key);
        this.logger.log("Removed from local storage: " + key);
    }

}
